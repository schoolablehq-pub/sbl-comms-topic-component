const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const baseWebpackConfig = require('./webpack.base.conf');

module.exports = merge(baseWebpackConfig, {
   entry: './dev/index.js',
   plugins: [
       new HtmlWebpackPlugin({
           template: './dev/index.html',
           inject: true,
           filename: 'index.html'
       })
   ],
   devServer: {
        contentBase: path.join(__dirname, './dev'),
        compress: true,
        port: 9000
    },
    watch: true
});