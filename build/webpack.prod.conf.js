const merge = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const baseWebpackConfig = require('./webpack.base.conf');

module.exports = merge(baseWebpackConfig, {
  entry: './src/index.js',
  output: {
    filename: 'sbl-comms-topic-component.min.js',
    library: 'CommsTopicComponent',
    libraryTarget: 'umd'
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        sourceMap: true,
        terserOptions: {
          compress: {
            warnings: false,
            drop_debugger: true,
            drop_console: true,
          },
          output: {
            comments: false
          }    
        },
      }),
    ],
  },
  externals: {
    'date-fns': 'date-fns',
    'vue-functional-calendar': 'vue-functional-calendar',
    'vue-markdown': 'vue-markdown',
    'pusher-js': 'pusher-js'
  },
});