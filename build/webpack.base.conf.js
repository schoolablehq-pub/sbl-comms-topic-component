const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const env = process.env.NODE_ENV === 'production' ? 'production' : 'development'

const supportedLocales = ['en'];

module.exports = {
    mode: env,
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].js',
        publicPath: '/'
    },
    devtool: env === 'production' ? 'source-map' : 'eval-source-map',
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            '@': path.resolve(__dirname, '../src'),
            'mixins': path.resolve(__dirname, '../src/mixins'),
            'components': path.resolve(__dirname, '../src/components'),
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
              },
              {
                test: /\.js$/,
                loader: 'babel-loader',
                include: path.resolve(__dirname, '../'),
                exclude: /node_modules/,
              },
              {
                test: /\.s?css$/,
                use: [
                    env === 'production' ? MiniCssExtractPlugin.loader : 'vue-style-loader',
                  'css-loader',
                  'postcss-loader',
                  'sass-loader',
                ],
              },
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: 'sbl-comms-topic-component.min.css',
        }),
        new webpack.DefinePlugin({
            'process.env': env,
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.ContextReplacementPlugin(
            /date\-fns[\/\\]/,
            new RegExp(`[/\\\\\](${supportedLocales.join('|')})[/\\\\\]`)
        )
    ],
    stats: {
        modules: false
    }
};