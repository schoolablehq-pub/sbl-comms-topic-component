import SblCommsTopic from './components/Topic.vue';
import SblCommsTopicComments from './components/TopicComments.vue';

export default {
    install(Vue) {
        Vue.component('comms-topic', SblCommsTopic);
        Vue.component('comms-topic-comments', SblCommsTopicComments);
    }
};