export default {
  methods: {
    /**
     * Get the component to render a piece of the topic's
     * content based on the content type. e.g image, video
     *
     * @param {String} type
     * @return {Object}
     */
    getContentTypeComponent(type) {
      if (!type) {
        return null;
      }

      return `${this.capitalize(type)}ContentType`;
    },

    /**
     * Capitalize the first character in the value.
     *
     * @type {String} value
     * @return {String}
     */
    capitalize(value) {
      return value.charAt(0).toUpperCase() + value.slice(1);
    },

    /**
     * Generate initials based on the given value.
     *
     * @param {String} value
     * @return {String}
     */
    initials(value) {
      const stopWords = ["the", "a", "is", "are"];

      let split = value.split(" ");

      if (split.length === 1) {
        return split[0]
          .split("")
          .slice(0, 2)
          .join("")
          .toUpperCase();
      }

      return split
        .filter(word => !stopWords.includes(word.toLowerCase()))
        .slice(0, 2)
        .map(word => word[0])
        .join("")
        .toUpperCase();
    }
  }
}