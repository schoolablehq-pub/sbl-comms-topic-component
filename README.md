# Schoolable Comms Topic Component

> Topic and Comments components for Schoolable Comms Service

# Requirements
- [Vue.js](https://vuejs.org) `^2.6.10`

# Installation

## Install the package as a dependency using yarn or npm

```shell
$ yarn add https://gitlab.com/schoolablehq-pub/sbl-comms-topic-component#0.0.2
```

```shell
$ npm install https://gitlab.com/schoolablehq-pub/sbl-comms-topic-component#0.0.2
```

Make sure to tag the appropriate release version.

## Register the components

```javascript
import Vue from 'vue';
import SblCommsTopicComponent from 'sbl-comms-topic-component';

Vue.use(SblCommsTopicComponent);
```

# Usage
```html
<div>
    <!-- Topic Component -->
    <comms-topic :topic="{}"></comms-topic>

    <!-- Comments Component -->
    <comms-topic-comments
    :comments="{}"
    :is-loading-comments="true"></comms-topic-comments>
</div>
```

If you would like to use the default styles, import it in the component where you're using the topics component.

```javascript
import "sbl-comms-topic-component/dist/sbl-comms-topic-component.min.css";
```

# Available Props

## Topic Component
| Prop  | Type   | Default | Required | Description         |
|-------|--------|---------|----------|---------------------|
| topic | Object |         | True     | The topic to render |

## Comments Component

### Props

| Prop                      | Type     | Default    | Required | Description                                                                                                                     |
|---------------------------|----------|------------|----------|---------------------------------------------------------------------------------------------------------------------------------|
| comments                  | Array    |            | true     | The comments to render (in reverse order)                                                                                                          |
| topic-id                  | String    |            | true     | The ID of the topic that has these comments                                                                                                     |
| comments-count            | Number   |            | false    | Total number of comments                                                                                                        |
| is-loading-comments       | Boolean  |            | true     | Indicate loading state for this component                                                                                       |
| can-comment               | Boolean  | true       | false    | Indicate whether or not the user can comment                                                                                    |
| schoolable-id             | String   |            | false    | The schoolable user ID of the current user                                                                                      |
| max-scroll-top            | Number   | 200        | false    | Specifies the maximum distance between the top of the comments container and a position that triggers the scroll listener event |
| scroll-delay              | Number   | 400        | false    | Number of milliseconds before the scroll event fires                                                                            |
| send-comment              | Function | See source | false    | Sends a new comment                                                                                                             |
| retry-sending-comment     | Function | See source | false    | Attempts to resent a new comment if it fails                                                                                    |
| scroll-comments-into-view | Function | See source | false    | Scrolls the comments container into view on first load |
| pusher-key | String |  | false    | API key for Pusher [Pusher](https://pusher.com/)

### Events

| Event           | Payload                                           | Description                                                                                                |
|-----------------|---------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| send-comment    | { text: 'Text to send' }                          | Emits when a new comment is being sent                                                                     |
| send-comment    | { text: 'Text to re-send', clientIdentifier: 123} | Emits when a failed comment is being resent.                                                               |
| scrolled-to-top |                                                   | Emits when the comments container is scrolled to the top. This is an indication to load previous comments. |
| new-comment |      Object                                             | Emits when a new comment comes in in real-time. This is usually an indication to update the comments list. |

Note: For the second event to trigger, add two ephemeral properties to the comment object when you add it to the comments array immediately it's being sent: `isSending` and `success`. These indicate whether a comment is in the process of sending and whether or not it was successful respectively. For the latter, a retry button would be visible if `success` becomes false in the comment object. The former indicates a loading state for the comment being sent.

# Slots

`header`
```html
<slot name="header">
    <div class="comments__header">
        <h2 style="font-size: 1.25rem;">
            Comments
            <small v-if="commentsCount" class="badge badge-soft-info">{{ commentsCount }}</small>
        </h2>
    </div>
</slot>
```

`loading`
```html
<slot name="loading">
    <h4>Loading...</h4>
</slot>
```

`single-comment`

Scope:
- comment {Object} - A single comment

```html
<slot name="single-comment" v-bind="comment">
    <topic-single-comment
        v-bind:comment="comment"
        :class="getCommentClass(comment, index)"
        :schoolable-id="schoolableId"
        @retry-last-comment.capture="retrySendingComment"
    />
</slot>
```

`no-comments`
```html
<div>
    <h4>No comments available</h4>
</div>
```

`comment-input`
```html
<div class="row align-items-center">
    <div class="col">
        <input
        type="text"
        v-model="newComment"
        class="form-control comments__input border-0"
        placeholder="Say something..."
        data-toggle="autosize"
        v-on:keyup.enter="sendComment"
        />
    </div>
</div>
```

Topic Slots

`header`

Scope:
- topic {Object} - The topic object

```html
<slot v-bind:topic="topic" name="header">
    <div class="row align-items-center">
        <div class="col-auto">
            <div class="sbl-comms-topic__avatar avatar avatar-xs" v-if="topic.source.logo_url">
        <img :src="topic.source.logo_url" class="avatar-img rounded-circle" />
            </div>
            <div v-else class="sbl-comms-topic__avatar avatar">
            <span
            class="avatar-title rounded-circle"
            style="color: #2c7be5; background-color: #d5e5fa;"
        >{{ initials(topic.source.name) }}</span>
            </div>
        </div>
        <div class="col ml-n2">
            <h4 class="sbl-comms-topic__source-name mb-0">{{ topic.source.name }}</h4>
            <small
            class="sbl-comms-topic__source-author"
            v-if="topic.author"
            >{{ topic.author.first_name }} {{ topic.author.last_name }}</small>
        </div>
    </div>
</slot>
```

`body`

Scope:
- body {Array} - The topic contents

```html
<slot v-bind:body="topic.body.content" name="body">
    <div class="mt-3">
        <div class="sbl-comms-topic__body">
        <component :is="topicTypeComponent" :topic="topic" />
        </div>
    </div>
</slot>
```

# License

[The MIT License](http://opensource.org/licenses/MIT)
